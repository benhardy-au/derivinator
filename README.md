# derivinator

This whirling pile of code is pretty much a fancy data structure (AST) to
hold algebraic expressions and produce their derivatives.

This is learning effort on my part on my way to actually
understanding differential calculus and partial derivatives,
on the way to building some proper neural networks from
scratch.

Basically learning this stuff by teaching a computer to do it. It was
really fun having all these pieces just click into place like Lego.

The sneaky actually ultimate goal is to really calculate derivatives of sigmoid functions. Which it can
do now so really it's all gravy from here.

## What can it do?

* calculate derivatives of expressions containing constants, polynomials, products, sums, e^x
* chain rule evaluation. what a concept!
* basic consolidation of obvious things like constants and polynomials getting multiplied together

Look at the unit tests and you will see fun stuff like:
```scala
    describe("derivative") {
      it("a less simple example") {
        val ex = Exponent(x)
        val input = (3 * x) / (Exponent(x))

        // we don't have factoring capabilities yet so this is as good as it gets for now
        val expected = ((3 * ex) - (3 * x) * ex) / (ex ^2)
        val derivative = input.derivative
        assert(derivative == expected)
      }
      it("a sigmoid function") {
        val enx = Exponent(-x)
        val input = 1 / (1 + enx)

        val expected = enx / ((1 + enx) ^2)
        val derivative = input.derivative
        assert(derivative == expected)
      }
    }
```
## What can't it do, that one might reasonably expect in a derivative generator?

Factoring. It's spit out some silly-looking but correct expressions as you can see in
the expectations for the above test.

I suspect adding tons of
code to make results look pretty might make the code a lot less pretty, and the point here is to
show a way for derivatives to get generated.

I also haven't bothered implementing derivatives of trig functions or logarithms yet.

## Imagined FAQ

### 1. Why did you write this in Scala?

Case classes, pattern matching, operators, typesafe, runs on
a JVM, avoids Python.

### 2. This looks inefficient.

It's not written to be fast, it's written to teach
myself things without getting bogged down in irrelevant details.

Anyway there's no `main()` entrypoint, just a bunch of unit tests, so...

### 3. Can I suggest something?

Yes! But please bear in mind that this is a learning
exercise for me so being handed stuff on a plate may
be counterproductive.

PRs are welcome.

### 4. What's your source material?

I basically read this [intro to differential calculus](https://www.sydney.edu.au/content/dam/students/documents/mathematics-learning-centre/introduction-to-differential-calculus.pdf) from our
friends at Sydney Uni, did the exercises on it, and started coding.

## Feedback

If you play with this and have any thoughts or ideas on it, please
let me know - I'm on twitter at [@hardycoding](https://twitter.com/hardycoding). Cheers!

## Thanks

To my good mate [Dr. Mark Greenaway](https://twitter.com/certifiedwaif) who is encouraging, guiding and inspiring a great deal here.
