name := "derivinator"

version := "0.1"

scalaVersion := "3.1.0"

idePackagePrefix := Some("net.bhardy.maths.derivinator")

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.10" % Test,
  "org.scalatest" %% "scalatest-funspec" % "3.2.10" % Test
)