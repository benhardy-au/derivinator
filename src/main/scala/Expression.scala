package net.bhardy.maths.derivinator

import Constant.One

trait Atom {}

sealed abstract class Expression {
  def derivative: Expression

  def +(rhs: Expression): Expression = rhs match {
    case Zero => this // adding 0 has no effect
    case r if r == this => r * 2
    case r => Sum(this, r)
  }
  def -(rhs: Expression): Expression = rhs match {
    case Zero => this // subtracting 0 has no effect
    case r => Sum(this, Negative(r))
  }

  def *(rhs: Expression): Expression = rhs match {
    case Zero => Zero // multiplying by 0 gives 0
    case One => this // multiplying by 1 has no effect
    case Negative(r) => Negative(this * r)
    case c:Constant => c * this
    case r if r == this => Poly(r, 2)
    case Poly(r, p) if r == this => Poly(r, p + 1)
    case r => Product(this, r)
  }

  def ^(rhs: Double): Expression = rhs match {
    case 0.0 => One // powering by 0 make 1
    case 1.0 => this // powering by 1 has no effect
    case r => Poly(this, r)
  }

  def /(divisor: Expression): Expression = divisor match {
    case Zero => throw new IllegalArgumentException("cannot divide by 0")
    case One => this // powering by 1 has no effect
    case d => Quotient(this, d)
  }

  def unary_- : Expression = Negative(this)
}

object Expression {
  implicit def toConst(x: Double): Expression = Constant(x)
}

case object Zero extends Expression with Atom {
  override def derivative = Zero

  override def toString = "0"

  override def +(rhs: Expression): Expression = rhs

  override def -(rhs: Expression): Expression = rhs match {
    case Negative(term) => term
    case other => Negative(other)
  }

  override def *(rhs: Expression): Expression = Zero

  override def unary_- : Expression = Zero
}


case class Constant(value: Double) extends Expression with Atom {
  override def derivative: Expression = Zero

  override def toString = Constant.numFormat(value)

  override def +(rhs: Expression): Expression = rhs match {
    case Zero => this
    case Constant(k) => Constant(k + this.value)
    case other => Sum(this, other)
  }

  override def *(rhs: Expression): Expression = (this, rhs) match {
    case (me, Constant(other)) => Constant(me.value * other)
    case (_, Zero) => Zero
    case (me, One) => me
    case (One, rhs) => rhs
    case (me, Product(Constant(k), prodRight)) => (me.value * k) * prodRight
    case (me, Product(prodLeft, Constant(k))) => (me.value * k) * prodLeft
    case (_, other) => super.*(other)
  }
}

object Constant {
  val One = new Constant(1)

  case object e extends Expression with Atom {
    override def derivative: Expression = Zero

    override def toString = "e"

    def ^ (rhs: Expression): Expression = rhs match {
      case Zero => Zero
      case One => this
      case other => Exponent(other)
    }
  }

  def apply(value: Double): Expression = value match {
    case 0.0 => Zero
    case 1.0 => One
    case x if x < 0.0 => Negative(new Constant(-x))
    case other => new Constant(other)
  }

  def numFormat(value: Double): String = {
    if (value.isWhole)
      value.toInt.toString
    else
      value.toString
  }
}

case class Argument(name: String) extends Expression with Atom {
  override def derivative: Expression = One

  override def toString = name
}

case class Sum(left: Expression, right: Expression) extends Expression {
  override def derivative: Expression = left.derivative + right.derivative

  override def toString = right match {
    case Negative(rhs) => s"$left - $rhs"
    case _ => s"$left + $right"
  }
}

case class Product(left: Expression, right: Expression) extends Expression {
  override def derivative: Expression = {
    (left * right.derivative) + (left.derivative * right)
  }

  override def +(rhs: Expression) = rhs match {
    case newRight if newRight == right => Product(left + 1, newRight)
    case Product(newLeft, newRight) if newRight == right =>
      Product(left + newLeft, newRight)
    case Product(newLeft, newRight) if newLeft == left =>
      Product(newLeft, right + newRight)
    case other => super.+(other)
  }

  def decorate(exp: Expression): String = exp match {
    case s: Sum => s"($s)"
    case other => other.toString
  }

  override def toString = (left, right) match {
    case (a: Atom, b: Atom) => s"$a$b"
    case _ => s"${decorate(left)} ${decorate(right)}"
  }
}

case class Quotient(top: Expression, bottom: Expression) extends Expression {
  override def derivative: Expression = {
    ((top.derivative * bottom) - (top * bottom.derivative)) /
      (bottom ^ 2)
  }

  def decorate(exp: Expression): String = exp match {
    case a: Atom => a.toString
    case other => s"($other)"
  }

  override def toString = s"${decorate(top)}/${decorate(bottom)}"
}

case class Poly(exp: Expression, power: Double) extends Expression {
  override def derivative: Expression = {
    if (power == 0.0)
      Zero
    else if (power == 1.0)
      One
    else {
      // via chain rule
      Constant(power) * exp.derivative * (exp ^ (power - 1))
    }
  }

  override def *(other : Expression) = other match {
    case Poly(r, rp) if r == exp => Poly(exp, power + rp)
    case rhs if rhs == exp => Poly(rhs, power + 1)
    case other => super.*(other)
  }

  override def toString = {
    val pow = Constant.numFormat(power)
    exp match {
      case atom: Atom => s"$atom^$pow"
      case other => s"($other)^$pow"
    }
  }
}

/**
 * e^x
 */
case class Exponent(power: Expression) extends Expression {
  override def derivative = {
    power.derivative * this
  }

  override def toString = {
    power match {
      case a: Atom => s"e^$a"
      case composite => s"e^($composite)"
    }
  }
}

case class Negative(expression: Expression) extends Expression {
  override def derivative: Expression = Negative(expression.derivative)

  override def toString = {
    expression match {
      case atom: Atom => s"-$atom"
      case composite => s"-($composite)"
    }
  }

  override def unary_- = expression

  override def *(other: Expression): Expression = other match {
    // cases for cancelling out negatives
    case Negative(nelly) => expression * nelly
    case Product(Negative(l), other) => expression * (l * other)
    case Product(other, Negative(r)) => expression * (other * r)
    // pull negatives out front
    case anything => Negative(expression * anything)
  }
}
