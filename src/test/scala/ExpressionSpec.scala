package net.bhardy.maths.derivinator

import Constant.{One, e}

import org.scalatest.funspec.AnyFunSpec

class ExpressionSpec extends AnyFunSpec {
  describe("Zero") {
    describe("derivative") {
      it("should be Zero") {
        assert(Zero.derivative == Zero)
      }
    }
    describe("toString") {
      it("should be \"0\"") {
        assert(Zero.toString == "0")
      }
    }
    describe("addition") {
      it("should return rhs when on lhs") {
        val five = Constant(5)
        assert(Zero + five == five)
      }
      it("should return lhs when on rhs") {
        val five = Constant(5)
        assert(five + Zero == five)
      }
    }
    describe("multiplication") {
      it("should return Zero when on lhs") {
        val five = Constant(5)
        assert(Zero * five == Zero)
      }
      it("should return Zero when on rhs") {
        val five = Constant(5)
        assert(five * Zero == Zero)
      }
    }
  }

  describe("Constant") {
    describe("derivative") {
      it("should be Zero") {
        val constant: Expression = Constant(4)
        assert(constant.derivative == Zero)
      }
    }
    describe("toString") {
      it("should be numeric value as integer when whole") {
        val constant: Expression = Constant(4)
        assert(constant.toString == "4")
      }
      it("should be numeric value as real when not whole") {
        val constant: Expression = Constant(4.56)
        assert(constant.toString == "4.56")
      }
    }
    describe("factory method Constant.apply") {
      it("should collapse 0s to Zero") {
        val constant: Expression = Constant(0.0)
        assert(constant == Zero)
      }
      it("should collapse 1s to One special case") {
        val constant: Expression = Constant(1)
        assert(constant == One)
      }
      it("should wrap negatives in Negative") {
        val constant: Expression = Constant(-7)
        assert(constant == Negative(Constant(7)))
      }
      it("should wrap everything else as-is") {
        val constant: Expression = Constant(7)
        assert(constant == Constant(7))
      }
    }
    describe("multiplication") {
      describe("with Products containing constants") {
        it("should factor out constants on lhs of product") {
          val x = Argument("x")
          val actual = Constant(7) * Product(3, x)
          val expected = 21 * x
          assert(actual == expected)
        }
        it("should factor out constants on rhs of product") {
          val x = Argument("x")
          val actual = Constant(7) * Product(x, 11)
          val expected = 77 * x
          assert(actual == expected)
        }
      }
      describe("with other Constants") {
        it("should just combine into a new constant") {
          val x = Argument("x")
          val actual = Constant(7) * Constant(3) * x
          val expected = 21 * x
          assert(actual == expected)
        }
      }
    }
    describe("addition") {
      describe("to other constants") {
        it("should just collapse the sum into a single constant") {
          assert(Constant(42) + Constant(7) == Constant(49))
        }
      }
    }
  }

  describe("Argument") {
    describe("derivative") {
      it("should be 1") {
        val arg = Argument("x")
        assert(arg.derivative == One)
      }
    }
    describe("toString") {
      it("should be the Argument's name") {
        val arg: Expression = Argument("x")
        assert(arg.toString == "x")
      }
    }
  }

  describe("Sum") {
    val x = Argument("x")
    describe("derivative") {
      describe("of a Sum of things whose derivatives are 0") {
        it("should be 0") {
          val expr = Sum(Zero, Constant(5))
          assert(expr.derivative == Zero)
        }
      }
      describe("of a Sum of polynomials") {
        it("should be sum of components' derivatives") {
          val expr = (x^5) + (3 * (x^4)) + (7 * (x^2)) - x + 9
          val actual = expr.derivative
          val expect = (5*(x^4)) + (12*(x^3)) + (14 * x) - 1
          assert(actual == expect)
        }
      }
      describe("with Argument and Constant") {
        it("should be sum of components' derivatives") {
          val exp = Sum(x, Constant(5))
          assert(exp.derivative == One)
        }
      }
    }
    describe("toString") {
      it("should show left then plus then right") {
        val exp = Sum(x, Constant(5))
        assert(exp.toString == "x + 5")
      }
      it("should show chains of sums as simple expressions with pluses, not nested parens") {
        val exp = x + 5 + (x^3) + (3*(x^2))
        assert(exp.toString == "x + 5 + x^3 + 3 x^2")
      }
    }
  }

  describe("Product") {
    val x = Argument("x")
    describe("derivative") {
      describe("of a product of a constant and an argument") {
        it("should be that constant") {
          val const = Constant(5)
          val exp = const * x
          val actual = exp.derivative
          val expected = const
          assert(actual == expected)
        }
      }
      describe("of a product of an argument and a sum") {
        it("should flatten the argument to 1") {
          val original = 5 * (x + 1)
          val expected: Expression = 5
          assert(original.derivative == expected)
        }
      }
      describe("of a product of an Argument and a Sum with a Poly") {
        describe("should use chain rule") {
          val original = 5 * ((x ^ 3) + 1)
          val expected: Expression = 15 * (x ^ 2)
          assert(original.derivative == expected)
        }
      }
    }
    describe("toString") {
      it("should show left then right") {
        val exp = 5 * x
        assert(exp.toString == "5x")
      }
      it("should parenthesize lower precedence arguments") {
        val exp = (5 * x) * (x + 4)
        assert(exp.toString == "5x (x + 4)")
      }
      it("should drop spaces for constants x inputs") {
        val exp = 5 * x
        assert(exp.toString == "5x")
      }
      it("should not drop spaces for constants x constants") {
        val exp = Constant(5) * 6
        assert(exp.toString == "30")
      }
    }
  }

  describe("Poly") {
    val x = Argument("x")
    describe("derivative") {
      it("should be Zero when power is Zero") {
        assert(Poly(x, 0).derivative == Zero)
      }
      it("should be One when power is One") {
        assert(Poly(x, 1).derivative == One)
      }
      it("should be n x^(n-1) otherwise") {
        val poly = Poly(x, 5)
        val expectedDerivative = Product(Constant(5), Poly(x, 4))
        assert(poly.derivative == expectedDerivative)
      }
      it("should apply chain rule for composite expressions") {
        import Expression.toConst
        val poly: Expression = (3 * x + 1) ^ 7
        val expectedDerivative = 21 * ((3 * x + 1) ^ 6)
        assert(poly.derivative == expectedDerivative)
      }
    }
    describe("multiplication") {
      it("should combine polynomials with terms they contain instead of creating a Product") {
        val result = Poly(x + 1, 4) * (x + 1)
        val expect = Poly(x + 1, 5)
        assert(result == expect)
      }
      it("should combine with other polynomials with same terms") {
        val result = Poly(x + 1, 4) * Poly(x + 1, 3)
        val expect = Poly(x + 1, 7)
        assert(result == expect)
      }
    }
    describe("toString") {
      it("should just show argument^power") {
        assert(Poly(x, 5).toString == "x^5")
      }
    }
  }

  describe("Exponent") {
    val x = Argument("x")
    describe("derivative") {
      it("should uses chain rule for composite argument") {
        assert((e ^ (2 * x)).derivative == 2 * (e^(2 * x)))
      }
      it("uses chain rule for compsite poly arguments") {
        val original = e ^ (x ^ 2)
        val expected = 2 * x * (e ^ (x ^ 2))
        val actual = original.derivative
        assert(actual == expected)
      }
    }
    describe("toString") {
      describe("with atomic parameters") {
        it("should not parenthesize arguments") {
          assert((e ^ x).toString == "e^x")
        }
        it("should not parenthesize constants") {
          assert((e ^ 2).toString == "e^2")
        }
      }
      describe("with non-atomic parameters") {
        it("should add parentheses") {
          assert((e ^ (x + 3)).toString == "e^(x + 3)")
        }
      }
    }
  }

  describe("Negative") {
    val x = Argument("x")
    describe("derivative") {
      it("should negate the argument's derivative") {
        assert(Negative(2 * (x ^ 3)).derivative == Negative(6 * (x ^ 2)))
      }
    }
    describe("multiplication") {
      describe("when on right hand size") {
        describe("using * operator") {
          it("should make a negative product if RHS is negative") {
            val result = 3 * (-x)
            assert(result == Negative(Product(3, x)))
          }
        }
      }
    }
    describe("toString") {
      it("should parenthesize Atomic parameters") {
        assert(Negative(x).toString == "-x")
      }
      it("should not parenthesize non-Atomic parameters") {
        assert(Negative(x + 3).toString == "-(x + 3)")
      }
    }
  }

  describe("Quotient") {
    val x = Argument("x")
    describe("derivative") {
      it("should handle hyperbolic functions") {
        val expression = 1 / x
        val expected = -1 / (x ^ 2)
        val derivative = expression.derivative
        assert(derivative == expected)
      }
      it("should handle composite numerators and denominators") {
        val input = (3 * x) / (e^ x)

        // we don't have factoring capabilities yet so this is as good as it gets for now
        val expected = ((3 * (e^ x)) - (3 * x) * (e^ x)) / ((e^ x) ^2)
        val derivative = input.derivative
        assert(derivative == expected)
      }
      it("should handle a sigmoid function") {
        val input = 1 / (1 + (e^(-x)))

        val expected = (e^(-x)) / ((1 + (e^(-x))) ^2)
        val derivative = input.derivative
        assert(derivative == expected)
      }
    }
    describe("toString") {
      it("should not parenthesize Atomic parameters") {
        assert((1 / x).toString == "1/x")
      }
      it("should parenthesize non-Atomic parameters") {
        assert((1 / (x + 1)).toString == "1/(x + 1)")
      }
    }
  }

  describe("Base expression operator") {
    describe("*") {
      val x = Argument("x")
      it("should turn multiplication of equivalent terms into a polynomial") {
        val result = (e^(x + 1)) * (e^(x + 1))
        val expect = (e^(x + 1)) ^ 2
        assert(result == expect)
      }
      it("should combine terms appearing in Polys containing them instead of creating a Product") {
        val result = (x + 1) * Poly(x + 1, 4)
        val expect = Poly(x + 1, 5)
        assert(result == expect)
      }
      it("should put Constants on the left side of the product") {
        val result = (x + 1) * 4
        val expect = Product(4, x + 1)
        assert(result == expect)
      }
    }
    describe("+") {
      val x = Argument("x")
      it("should turn addition of equivalent terms into a product") {
        val result = (e^(x + 1)) + (e^(x + 1))
        val expect = 2 *(e^(x + 1))
        assert(result == expect)
      }
      it("should combine terms appearing in Products containing them instead of creating a Product") {
        val result = (3 * (x + 1)) + (7 * (x + 1))
        val expect = 10 * (x + 1)
        assert(result == expect)
      }
      it("should put Constants on the right side of the sum") {
        val result = (x ^ 2) + 4
        val expect = Sum(x ^ 2, 4)
        assert(result == expect)
      }
    }
  }
}
